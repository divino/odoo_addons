# -*- coding: utf-8 -*-
# © 2012-2015 KMEE (http://www.kmee.com.br)
# @author Luis Felipe Miléo <mileo@kmee.com.br>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
# Alterações por Alexandre Defendi

from __future__ import with_statement

import logging
import base64
import odoo
from odoo.exceptions import UserError
from odoo.report.render import render
from odoo.report.interface import report_int
from ..boleto.document import Boleto

_logger = logging.getLogger(__name__)

class external_pdf(render):
    def __init__(self, pdf):
        render.__init__(self)
        self.pdf = pdf
        self.output_type = 'pdf'

    def _render(self):
        return self.pdf


class ReportCustom(report_int):
    """
        Custom report for return boletos
    """

    def create(self, cr, uid, ids, datas, context=False):
        env = odoo.api.Environment(cr, uid, context or {})

        active_ids = context.get('active_ids')
        ids_move_lines = []
        aml_obj = env['account.move.line']
        pol_obj = env['payment.order.line']
        AtachObj = env['ir.attachment']
        if active_ids:
            ids_move_lines = active_ids
        else:
            raise UserError(u'Parâmetros inválidos')

        boleto_list = aml_obj.browse(ids_move_lines).action_register_boleto()
        
        if not boleto_list:
            raise UserError(
                u"""Error
Não é possível gerar os boletos
Certifique-se que a fatura esteja confirmada e o
forma de pagamento seja duplicatas""")
        pdf_string = Boleto.get_pdfs(boleto_list)
        self.obj = external_pdf(pdf_string)
        self.obj.render()
        
        payment_order = pol_obj.search([('move_line_id','=',active_ids[0])],order="id desc",limit=1).payment_order_id
        
        for move_line_id in ids_move_lines:
            move_line = aml_obj.browse(move_line_id)
            boleto = move_line.action_register_boleto()
            pdf_string = Boleto.get_pdfs(boleto)
#             pdf_obj = external_pdf(pdf_string)
#             pdf_obj.render()
            Vals_atach = {
                'name': u'boleto_%s.pdf' % boleto[0].nosso_numero,
                'db_datas': base64.encodestring(pdf_string),
                'datas_fname': u'boleto_%s.pdf' % boleto[0].nosso_numero,
                'res_model': 'account.move.line',
                'res_id': move_line_id,
                'partner_id': move_line.partner_id.id,
                'user_id': env.user.id,
                'company_id': env.user.company_id.id,
                'description': 'Boleto '+move_line.name + '/' + boleto[0].nosso_numero,
                'type': 'binary',
            }
            AtachObj.sudo().create(Vals_atach)
        
        return self.obj.pdf, 'pdf'


ReportCustom('report.br_boleto.report.print')
