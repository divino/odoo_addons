# -*- coding: utf-8 -*-
# © 2016 Alexandre Defendi, Arkan System
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': u'Nfe XML Import',
    'description': u"Módulo de Importação do XML de Notas Fiscais Eletrônicas de Produtos",
    'summary': u"Importação do XML de Notas Fiscais Eletrônicas de Produtos",
    'version': '0.0.4',
    'category': 'Localization',
    'author': 'Arkan System Team',
    'license': 'AGPL-3',
    'website': 'https://arkansystem.com.br/',
    'contributors': [
        'Alexandre Defendi <alexandre@arkansystem.com.br>',
        'Gilberto Santos <gilberto@arkansystem.com.br>'
        'Jhonatan Eduardo <jhonatan@arkansystem.com.br>',
        'Pablo Dunke <pablo@arkansystem.com.br>',
        'Rafael Harada <rafael@arkansystem.com.br>',
    ],
    'images' : [
    ],
    'depends': [
        'base',
        'web',
        'product',
        'purchase',
        # 'br_account',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/res_partner_view.xml',
        'views/import_xml_nfe_view.xml',
        #'views/account_invoice_view.xml',
        #'views/account_invoice_supplier_view.xml',
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'post_init_hook': False,
}
