# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api, tools
from odoo.exceptions import except_orm, Warning, RedirectWarning, ValidationError

class res_partner_product(models.Model):
    _name           = 'res.partner.product'
    _table          = 'res_partner_product'
    _order          = ''
    
    codigo          = fields.Char(string=u"Código", size=30)
    partner_id      = fields.Many2one(comodel_name="res.partner", string=u"Fornecedor")
    product_id      = fields.Many2one(comodel_name="product.product", string=u"Produto")

    _sql_constraints = [
        ('produto_uniq', 'unique(codigo,partner_id)',
            'Código do Fornecedor Já Cadastrado!'),
    ]


class res_partner(models.Model):
    _inherit = 'res.partner'

    ids_produto             = fields.One2many(comodel_name='product.supplierinfo', inverse_name='name')