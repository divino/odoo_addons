# -*- coding: utf-8 -*-
###############################################
# Xande 2016                                  #
#                                             #
###############################################

import logging
import tempfile
import datetime
import re
import chardet
import odoo.addons.decimal_precision as dp
from odoo import SUPERUSER_ID
from odoo import models, fields, api, tools, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
from xml.dom import minidom
#from mx.Tools.mxTools.test import items


_logger = logging.getLogger(__name__)

_TASK_STATE = [('draft', 'Nova'),('init', 'Iniciada'), ('done', 'Finalizada'),('erro','Erro')]

def NCMformate(NCM):
    if NCM:
        val = re.sub('[^0-9]', '', NCM)
        print val
        tamanho = len(val)
        print tamanho
        if tamanho > 2:
            if tamanho < 5:
                return "%s.%s" % (val[0:2], val[2:])
            elif tamanho < 7:
                NCM = "%s.%s" % (val[0:4], val[4:])
            else:
                NCM = "%s.%s.%s" % (val[0:4], val[4:6], val[6:])
        else:
            NCM = val
    else:
        NCM = ''
    return NCM

def ElementByTag(xml,tag,pos=False,data=False):
    if xml:
        element = xml.getElementsByTagName(tag)
        if element:
            if type(pos) is int:
                if data:
                    if element[pos].firstChild:                 
                        return element[pos].firstChild.data
                else:
                    return element[pos]
            else:
                return element
    return False

class import_xml_nfe(models.Model):
    """
        Classe de importação do XML da NFe
        1- Recebe o XML
        2- Preenche os campos da NFe de Compra
        3- Arquiva o XML
    """ 
    
    _name           = 'import.xml.nfe'
    _table          = 'import_xml_nfe'
    _description    = u'Wizard de Importação do XML da NFe de Compra'
#    _rec_name       = "cdm_display_name"
#     _inherit        = ['mail.thread', 'ir.needaction_mixin']
#    _order          = "cdm_date desc, cdm_nr desc"

    @api.one
    @api.constrains('input_file_fname')
    def _check_input_file_fname(self):
        if self.input_file:
            if not self.input_file_fname:
                raise ValidationError(u"Não é um Arquivo.")
        else:
            # Check the file's extension
            tmp = self.filename.split('.')
            ext = tmp[len(tmp)-1]
            if ext != 'xml':
                raise ValidationError(u"O arquivo deve ser um arquivo XML")
 
    @api.onchange('purchase_id')
    def change_purchase_id(self):
        if self.purchase_id:
            if self.purchase_id.fiscal_position_id: 
                self.fiscal_position = self.purchase_id.fiscal_position_id
            if self.purchase_id.payment_term_id:
                self.term_payment_id = self.purchase_id.payment_term_id
        else:
            self.fiscal_position = False
            self.term_payment_id = False
    
    name                = fields.Char('Número da NF-e', size=50)
    purchase_id         = fields.Many2one(comodel_name='purchase.order', string=u"Ordem de Compra", domain="[('state','=','approved')]")
    fiscal_position     = fields.Many2one("account.fiscal.position", string=u"Posição Fiscal")
    #TODO1 fiscal_position     = fields.Many2one("account.fiscal.position", string=u"Posição Fiscal",domain="[('type','=','input')]")
    term_payment_id     = fields.Many2one("account.payment.term", string=u"Forma de Pagamento")
    input_file          = fields.Binary('Arquivo XML', attachment=True, filters="*.xml", required=True)
    input_file_fname    = fields.Char(string='File name')
    danfe_file          = fields.Binary('Danfe', required=False)
    danfe_file_fname    = fields.Char(string='Danfe name')
    user_id             = fields.Many2one('res.users', 'Inserido por', track_visibility='onchange')
    state               = fields.Selection(_TASK_STATE, 'Situacao', default='draft', required=True)
    dataEmissao         = fields.Char(string=u"Data de Emissão")
    numeronNF           = fields.Char(string=u"Número da nNF")
    invoice_id          = fields.Many2one('account.invoice', u'Fatura', track_visibility='onchange')
    new_product         = fields.Boolean(string=u'Criar Produto?',help=u'Habilete para caso o produto que constante no XML mas não esteja cadastrado seja incluído automaticamente.',required=True,default=False)
    categ_id            = fields.Many2one('product.category', string=u'Categoria')

    @api.multi
    def prepara_DI(self,xmldoc):

        
        ElementByTag(xmldoc,"numeroDI",0)
        DI = {
            'name'                  : ElementByTag(xmldoc,"numeroDI",0),
            'date_registration'     : ElementByTag(xmldoc,"dataRegistro",0),
            'state_id'              : False,
            'location'              : False,
            'date_release'          : False,
            'type_transportation'   : ElementByTag(xmldoc,"viaTransporteCodigo",0),
            'afrmm_value'           : False,
            'type_import'           : ElementByTag(xmldoc,"caracterizacaoOperacaoCodigoTipo",0),
            'thirdparty_cnpj'       : False,
            'thirdparty_state_id'   : False,
            'line_ids'              : False,
        }
        
        return DI

#     invoice_line_id = fields.Many2one(
#         'account.invoice.line', u'Linha de Documento Fiscal',
#         ondelete='cascade', index=True)
#     name = fields.Char(u'Número da DI', size=10, required=True)
#     date_registration = fields.Date(u'Data de Registro', required=True)
#     state_id = fields.Many2one(
#         'res.country.state', u'Estado',
#         domain="[('country_id.code', '=', 'BR')]", required=True)
#     location = fields.Char(u'Local', required=True, size=60)
#     date_release = fields.Date(u'Data de Liberação', required=True)
#     type_transportation = fields.Selection([
#         ('1', u'1 - Marítima'),
#         ('2', u'2 - Fluvial'),
#         ('3', u'3 - Lacustre'),
#         ('4', u'4 - Aérea'),
#         ('5', u'5 - Postal'),
#         ('6', u'6 - Ferroviária'),
#         ('7', u'7 - Rodoviária'),
#         ('8', u'8 - Conduto / Rede Transmissão'),
#         ('9', u'9 - Meios Próprios'),
#         ('10', u'10 - Entrada / Saída ficta'),
#     ], u'Transporte Internacional', required=True, default="1")
#     afrmm_value = fields.Float(
#         'Valor da AFRMM', digits=dp.get_precision('Account'), default=0.00)
#     type_import = fields.Selection([
#         ('1', u'1 - Importação por conta própria'),
#         ('2', u'2 - Importação por conta e ordem'),
#         ('3', u'3 - Importação por encomenda'),
#     ], u'Tipo de Importação', default='1', required=True)
#     thirdparty_cnpj = fields.Char('CNPJ', size=18)
#     thirdparty_state_id = fields.Many2one(
#         'res.country.state', u'Estado',
#         domain="[('country_id.code', '=', 'BR')]")
#     exporting_code = fields.Char(
#         u'Código do Exportador', required=True, size=60)
#     line_ids = fields.One2many(
#         'br_account.import.declaration.line',
#         'import_declaration_id', 'Linhas da DI')
    
    @api.multi
    def prepara_invoice(self, vals):
        
        #print 'Cria Invoice Line ' + str(vals.get('versao'))
        
        journal = self.env['account.journal'].search([('type', '=', 'purchase'), ('company_id', '=', self.env.user.company_id.id)],limit=1)
        if not journal:
            raise UserError(u'Nenhum Diário de Compras Definido!',u'Defina o Diário de Compras para a empresa: "%s" (id:%d).' % (self.env.user.company_id.name, self.env.user.company_id.id))

        invoice = {
            'name': vals.get("name"),
            'origin': vals.get("origin"),
            'type': 'in_invoice',
            'fiscal_type': 'product',
            'ind_final': "0",
            'ind_pres': "0",
            'vendor_number': vals.get('num_fatura'),
            'vendor_serie': vals.get('serie'),
            'fiscal_document_id': False,
            'document_serie_id': False,
            'issuer': "0",
            'date_hour_invoice': vals.get('data_emissao'),
            'date_in_out': vals.get('data_Ent_Sai'),
#             'date_due': False,
#             'fiscal_category_id': vals.get('fiscal_category_id'),
            'reference': False,
            'account_id': vals.get('account_id'),
#             'invoice_line': lines,
            'partner_id': vals.get('partner_id'),
            'journal_id': journal[0].id,
            'currency_id': self.env.user.company_id.currency_id.id,
            'payment_term': vals.get('payment_term'),
            'fiscal_position': vals.get('fiscal_position'),
            'date_invoice': vals.get('data_emissao'),
            'company_id': self.env.user.company_id.id,
            'user_id': self.env.user.id,
#             'contract_id': False,
            'internal_number': vals.get('num_fatura'),
            'number': vals.get('num_fatura'),
            'state': 'draft',
            'supplier_invoice_number': vals.get('num_fatura'),
            'nfe_access_key': vals.get('chave'),
            'nfe_version': vals.get('versao'),
            'nfe_protocol_number': vals.get('protocolo'),
#             'reference_type': False,
#             'partner_bank_id': False,
            'comment': vals.get('observacao'),
#             'fiscal_comment': False,
        }

        if vals.get("dupVenc"):
            invoice["date_due"] = vals.get("dupVenc")
        
        return invoice
    
    @api.multi
    def prepara_invoice_line(self, vals):
        AcTaxCodeOb = self.env["account.tax"]

        invoice_line = {
            'sequence': vals.get('sequencia'),
            'partner_id': vals.get('partner_id'),
            'invoice_id': vals.get('invoice_id'),
            'product_id': vals.get('product_id'),
            'name': vals.get('name'),
            'company_id': self.env.user.company_id.id,
            'account_id': vals.get('account_id'),
            'account_analytic_id': False,
            'quantity': vals.get('qtdade'),
            'uos_id': vals.get('uos_id'),
            'price_unit': vals.get('unitario') or 0.0,
            'discount': vals.get('discount') or 0.0,
            'invoice_line_tax_id': False,
#             'ipi_base': 0,
            'fiscal_position': vals.get('fiscal_position'),
#             'fiscal_category_id': vals.get('fiscal_category_id'),
#             'ipi_base numeric': False, #  NOT NULL, -- Base IPI
#             'issqn_value': False, #  numeric NOT NULL, -- Valor ISSQN
#             'pis_percent': False, #  numeric NOT NULL, -- Perc PIS
#             'pis_base_other': False, #  numeric NOT NULL, -- Base PIS Outras
#             'icms_base_other': False, #  numeric NOT NULL, -- Base ICMS Outras
#             'icms_origin': False, #  character varying, -- Origem
#             'icms_st_base_type': False, #  character varying NOT NULL, -- Tipo Base ICMS ST
#             'icms_st_value': False, #  numeric NOT NULL, -- Valor ICMS ST
#             'freight_value': False, #  numeric, -- Frete
#             'ipi_base_other': False, #  numeric NOT NULL, -- Base IPI Outras
#             'icms_percent': False, #  numeric, -- Perc ICMS
#             'cofins_st_value': False, #  numeric NOT NULL, -- Valor COFINS ST
#             'icms_st': False, # _percent numeric, -- Percentual ICMS ST
#             'insurance_value': False, #  numeric, -- Valor do Seguro
#             'ii_iof': False, #  numeric NOT NULL, -- Valor IOF
#             'cofins_type': False, #  character varying NOT NULL, -- Tipo do COFINS
#             'ii_customhouse_charges': False, #  numeric NOT NULL, -- Depesas Atuaneiras
#             'cfop_id': False, #  integer, -- CFOP
#             'cofins_value': False, #  numeric NOT NULL, -- Valor COFINS
#             'pis_base': False, #  numeric NOT NULL, -- Base PIS
#             'pis_st': False, # _value numeric NOT NULL, -- Valor PIS ST
#             'other_costs_value': False, #  numeric, -- Outros Custos
#             'ipi_value': False, #  numeric NOT NULL, -- Valor IPI
#             'cofins_st_percent': False, #  numeric NOT NULL, -- Perc COFINS ST
#             'icms_st_base': False, #  numeric NOT NULL, -- Base ICMS ST
#             'icms_st_mva': False, #  numeric, -- MVA ICMS ST
#             'cofins_st_base': False, #  numeric NOT NULL, -- Base COFINS ST
#             'pis_st_percent': False, #  numeric NOT NULL, -- Perc PIS ST
#             'ipi_manual': False, #  boolean, -- IPI Manual?
#             'cofins_base_other': False, #  numeric NOT NULL, -- Base COFINS Outras
#             'issqn_base': False, #  numeric NOT NULL, -- Base ISSQN
#             'cofins_st_type': False, #  character varying NOT NULL, -- Tipo do COFINS ST
#             'ipi_percent': False, #  numeric NOT NULL, -- Perc IPI
#             'pis_st_type': False, #  character varying NOT NULL, -- Tipo do PIS ST
#             'pis_st_base': False, #  numeric NOT NULL, -- Base PIS ST
#             'issqn_type': False, #  character varying NOT NULL, -- Tipo do ISSQN
#             'cofins_base': False, #  numeric NOT NULL, -- Base COFINS
#             'icms_st_base_other': False, #  numeric NOT NULL, -- Base ICMS ST Outras
#             'ii_base numeric': False, #  NOT NULL, -- Base II
#             'pis_value': False, #  numeric NOT NULL, -- Valor PIS
#             'ipi_cst_id': False, #  integer, -- CST IPI
#             'date_invoice': False, #  date, -- Invoice Date
#             'ipi_type': False, #  character varying NOT NULL, -- Tipo do IPI
#              'fiscal_classification_id': False, #  integer, -- Classificação Fiscal
#             'issqn_percent': False, #  numeric NOT NULL, -- Perc ISSQN
#             'pis_cst_id': False, #  integer, -- CST PIS
#             'pis_type': False, #  character varying NOT NULL, -- Tipo do PIS
#             'discount_value': False, #  numeric, -- Vlr. Desconto
#             'price_gross': False, #  numeric, -- Vlr. Bruto
#             'icms_st_percent_reduction': False, #  numeric, -- Perc Redução de Base ICMS ST
#             'ii_value': False, #  numeric NOT NULL, -- Valor II
#             'cofins_percent': False, #  numeric NOT NULL, -- Perc COFINS
#             'icms_manual': False, #  boolean, -- ICMS Manual?
#             'cofins_cst_id': False, #  integer, -- CST PIS
#             'icms_base_type': False, #  character varying NOT NULL, -- Tipo Base ICMS
#             'cofins_manual': False, #  boolean, -- COFINS Manual?
#             'icms_cst_id': False, #  integer, -- CST ICMS
#             'issqn_manual': False, #  boolean, -- ISSQN Manual?
#             'service_type_id': False, #  integer, -- Tipo de Serviço
#             'icms_base': False, #  numeric NOT NULL, -- Base ICMS
#             'icms_percent_reduction': False, #  numeric, -- Perc Redução de Base ICMS
#             'product_type': False, #  character varying NOT NULL, -- Tipo do Produto
#             'pis_manual': False, #  boolean, -- PIS Manual?
#             'icms_value': False, #  numeric NOT NULL, -- Valor ICMS
#             'purchase_line_id': False, #  integer, -- Purchase Order Line
        }
        
        if vals.get('ncm_id'):
            invoice_line['fiscal_classification_id'] = vals.get('ncm_id')

        imposto = vals.get('imposto') 
        if imposto:
            if imposto.get('IPI'):
                ipi = imposto['IPI']
                
                if ipi.get('CST'):
                    cst = AcTaxCodeOb.search([("name","=",ipi["CST"]),("domain","=","ipi")])
                   
                    if cst:
                        invoice_line['ipi_base']        = float(ipi.get('vBC') or 0.0)
                        invoice_line['ipi_base_other']  = 0.0
                        invoice_line['ipi_value']       = float(ipi.get('vIPI') or 0.0)
                        invoice_line['ipi_manual']      = True
                        invoice_line['ipi_percent']     = float(ipi.get('pIPI') or 0.0)
                        invoice_line['ipi_cst_id']      = cst.id
                if vals.get('fiscal_type') and ipi.get('vIPI'):
                    if vals.get('fiscal_type') != '3':
                        invoice_line['price_unit'] = invoice_line['price_unit'] + float(ipi.get('uIPI') or 0.0)
                        

            if imposto.get('ICMS'):
                ICMS = imposto.get('ICMS')
                cst = ICMS.get('CST')
                if cst:
                    icm_cst = AcTaxCodeOb.search([("name","=",cst),("domain","=","icms")],limit=1)
                    if icm_cst:
                        invoice_line['icms_cst_id'] = icm_cst.id

            if imposto.get('PIS'):
                PIS = imposto.get('PIS')
                cst = PIS.get('CST')
                if cst:
                    pis_cst = AcTaxCodeOb.search([("name","=",cst),("domain","=","pis")],limit=1)
                    if pis_cst:
                        invoice_line['pis_cst_id'] = pis_cst.id

            if imposto.get('COFINS'):
                COFINS = imposto.get('COFINS')
                cst = COFINS.get('CST')
                if cst:
                    cofins_cst = AcTaxCodeOb.search([("name","=",cst),("domain","=","cofins")],limit=1)
                    if cofins_cst:
                        invoice_line['cofins_cst_id'] = cofins_cst.id
                    
        if not invoice_line.get("ipi_cst_id"): 
            ipi_cst = AcTaxCodeOb.search([("name","=","99"),("domain","=","ipi")])
            if ipi_cst:
                invoice_line['ipi_cst_id'] = ipi_cst.id 

        if not invoice_line.get("icms_cst_id"): 
            icms_cst = AcTaxCodeOb.search([("name","=","00"),("domain","=","icms")])
            if icms_cst:
                invoice_line['ipi_cst_id'] = icms_cst.id 

        if not invoice_line.get("cofins_cst_id"): 
            pis_cst = AcTaxCodeOb.search([("name","=","01"),("domain","=","cofins")])
            if pis_cst:
                invoice_line['cofins_cst_id'] = pis_cst.id 
        
        return invoice_line

    @api.multi
    def get_supplier(self, vals):
        #pdb.set_trace()
        #company = self.env['res.users'].company_id
        
        cnpj_cpf = vals.get('cnpj_cpf')
        if cnpj_cpf:
            val = re.sub('[^0-9]', '', cnpj_cpf)
            cnpj_cpf = "%s.%s.%s/%s-%s"\
            % (val[0:2], val[2:5], val[5:8], val[8:12], val[12:14])
            vals['cnpj_cpf'] = cnpj_cpf

        supplier_obj = self.env['res.partner']
        #procura pelo cnpj
        if cnpj_cpf:
            supplier = supplier_obj.search([('cnpj_cpf', '=', cnpj_cpf), ('supplier','=',True)], limit=1)
        if not supplier:
            if vals.get('name'):
                supplier = supplier_obj.search([('name', '=', vals.get('name')), ('supplier','=',True)])
        if not supplier:
            if vals.get('name'):
                supplier = supplier_obj.search([('legal_name', '=', vals.get('name')), ('supplier','=',True)])

        # Se não achou o cadastro verifica se foi indicado a posição fiscaL
        if not supplier and not self.fiscal_position:
            raise UserError(_(u'Quando novo fornecedor é obrigatório informar a posição fiscal para efeito de cadastro.'))

        # Procura o País, Estado e Cidade do Fornecedor
        country_obj = self.env['res.country']
        country = country_obj.search([('ibge_code', '=', vals.get('country_id'))],limit=1)
        if not country:
            raise UserError(_(u'País não localizado! Verifique se o país possui 1058 cód. do IBGE. (cod %s)' % vals.get('country_id')))
        
        if vals.get('state'):
            city_obj = self.env['res.state.city']
            state_obj = self.env['res.country.state']
            
            state = state_obj.search([("country_id","=",country.id),("code","=",vals["state"])],limit=1)
            
            if state:
                ibge_code = vals.get('city_id')[2:]
                if ibge_code:
                    city = city_obj.search([("state_id","=",state.id),('ibge_code', '=', ibge_code)],limit=1)
                else:
                    city = False
                
                #if company.state_id and state.id <> company.state_id
        
        if not supplier:
            #se nao existir cadastra
            vals['customer'] = False
            if country:
                vals['country_id'] = country.id                                
            else:
                vals['country_id'] = False
            if state:
                vals['state_id'] = state.id
            else:
                vals['state_id'] = False
            if city:
                vals['city_id'] = city.id
            else:
                vals['city_id'] = False
            vals['property_account_position_id'] = self.fiscal_position.id
            
            supplier =  supplier_obj.create(vals)
        else:
            #se existir verifica e altera os  dados se necessário
            avals = {}
            if country:
                if not vals.get('country_id') or vals['country_id'] <> country.id:
                    avals['country_id'] = country.id
            if state:
                if not vals.get('state_id') or vals['state_id'] <> state.id:
                    avals['state_id'] = state.id
            if city:
                if not vals.get('city_id') or vals['city_id'] <> city.id:
                    avals['city_id'] = city.id
            if vals.get('name') and vals['name'] <> supplier.name:
                avals['name'] = vals['name']
            if vals.get('legal_name') and vals['legal_name'] <> supplier.legal_name:
                avals['legal_name'] = vals['legal_name']
            if vals.get('street') and vals['street'] <> supplier.street:
                avals['street'] = vals['street']
            if vals.get('number') and vals['number'] <> supplier.number:
                avals['number'] = vals['number']
            if vals.get('district') and vals['district'] <> supplier.district:
                avals['district'] = vals['district']
            #TODO 3 if not supplier.partner_fiscal_type_id:
                #partner_fiscal_type = self.env['l10n_br_account.partner.fiscal.type'].search([('code','=','Contribuinte')],limit=1)
                #avals['partner_fiscal_type_id'] = partner_fiscal_type.id
            if vals.get('zip') and vals['zip'] <> supplier.zip:
                avals['zip'] = vals['zip']
            if vals.get('inscr_est') and vals['inscr_est'] <> supplier.inscr_est:
                avals['inscr_est'] = vals['inscr_est']
            if vals.get('supplier') and vals['supplier'] <> True:
                avals['supplier'] = True
            if vals.get('is_company') and vals['is_company'] <> True:
                avals['is_company'] = True
         
            if avals:
                supplier.write(avals)
                    
        return supplier

    @api.multi
    def put_ncm(self,ncm):
        res = {}
        fiscal_clas = self.env["product.fiscal.classification"]
        if ncm:
            val = {
                "name": ncm,
                "code": ncm,
                "type": "normal",
            }
            res = fiscal_clas.create(val)
        return res

    @api.multi
    def get_product(self, vals, cadastra=False):
        u""" Método de procura do produto para adição na Fatura Importada """
        # Declarações
        res         = vals.copy()
        produto     = False;
        ncm_id      = False;
        prodt_obj   = self.env['product.template']
        prod_obj    = self.env['product.product']
        prod_part   = self.env["product.supplierinfo"]
        fiscal_clas = self.env["product.fiscal.classification"]
        categ_int   = self.env["product.category"]

        # procura pelo código Fornecedor
        if vals.get("default_code"):
            produto_partner = prod_part.search([("product_code","=",vals.get("default_code")),('name','=',vals.get('partner_id'))], limit=1)
            if produto_partner:
                produto = prod_obj.search([('product_tmpl_id','=',produto_partner.product_tmpl_id.id)])

        # procura pelo codBarra
        if not produto:
            if vals.get('ean13'):
                produto = prod_obj.search([('barcode', '=', vals.get('ean13'))], limit=1)

        # procura pelo nome
        if not produto:
            if vals.get('name'):
                produto = prod_obj.search([('name', '=', vals.get('name'))], limit=1)
                
        if produto:
            res["name"] = produto.name
            res["product_id"] = produto.id
            res["account_id"] = produto.property_account_expense_id.id
            res["uos_id"] = produto.uom_id.id
            product_ncm = produto.fiscal_classification_id
            ncm_id = False
            if res.get('ncm') and product_ncm:
                ncm_prod_code = re.sub('[^0-9]', '', product_ncm.code)
                if res['ncm'] != ncm_prod_code:
                    res['msg'] = u'Item {seq} - O NCM {ncm1} na NFe do {nome}, não é o mesmo {ncm2}'.\
                               format(seq=res['nr_item'],ncm1=res['ncm'],nome=produto.name,ncm2=ncm_prod_code)
                    return res
                else:
                    ncm_id = product_ncm.id
            if not ncm_id:
                if res.get('ncm'):
                    ncm_id = fiscal_clas.search([("code","=",NCMformate(res.get('ncm')))],limit=1)
                    if not ncm_id:
                        ncm_id = fiscal_clas.search([("code","=",res.get('ncm'))],limit=1)
                if ncm_id:
                    res["ncm_id"] = ncm_id.id
                    product_ncm = ncm_id
                    produto.fiscal_classification_id = ncm_id
                    produto.sudo().write({})
            if not res.get("ncm_id") and res.get("ncm"):
                ncm = self.put_ncm(res["ncm"])
                if ncm:
                    res["ncm_id"] = ncm.id
        elif cadastra:    
            res["product_id"]   = False
            res["account_id"]   = False
            res["uos_id"]       = False
            res["categ_id"]     = self.categ_id.id if self.categ_id else False
            
            if not res.get("categ_id"):
                categ_id = categ_int.search([('parent_id','=',False)],limit=1,order='id asc')
                if not categ_id:
                    categ_id = categ_int.create({
                            'name': 'NFs Entrada',
                            'parent_id': False,
                            'properety_valuation': 'manual_periodic',
                        })
                if categ_id:
                    res["categ_id"] = categ_id.id 
                
            if res.get('ncm'):
                ncm_id = fiscal_clas.search([("code","=",NCMformate(res.get('ncm')))],limit=1)
                if ncm_id:
                    ncm_id = fiscal_clas.search([("code","=",res.get('ncm'))],limit=1)
            if ncm_id:
                res["ncm_id"] = ncm_id.id
            if not res.get("ncm_id") and res.get("ncm"):
                ncm = self.put_ncm(res["ncm"])
                if ncm:
                    res["ncm_id"] = ncm.id

            prod_dict = {
                'name'          : res.get('name'),
                'type'          : 'product',
                'company_id'    : self.env.user.company_id.id,
                'cost_method'   : 'average',
                'valuation'     : 'real_time',
                'categ_id'      : res.get('categ_id'),
                'fiscal_classification_id': res.get('ncm_id'),
                'default_code'  : res.get('default_code'),
                'origin'        : res.get('origin'),
                'cest'          : res.get('cest'),
                'standard_price': res['unitario'] if res.get('unitario',False) else 0.0,
            }
            
            if res.get('ean13'):
                prod_dict['barcode'] =  res['ean13']
            
            if hasattr(prod_obj, 'mad_envia'):
                prod_dict['mad_envia'] =  False
  
            produto_tmp =  prodt_obj.create(prod_dict)
            if produto_tmp:
                produto = prod_obj.search([('product_tmpl_id','=',produto_tmp.id)])
                res["product_id"] = produto.id
        return res


    @api.multi
    def nfe_import(self):
        
        # Cria os Objetos a serem utilizados
        AtachObj        = self.env['ir.attachment']
        InvoiceObj      = self.env["account.invoice"]
        InvoiceLineObj  = self.env["account.invoice.line"]
        CategoriaObj    = self.env['product.category']
        FiscalPosObj    = self.env["account.fiscal.position"]
        
        # FiscalType      = self.env.user.company_id.fiscal_type

        for chain in self:
            
            chain.state = 'init'
            
            # Cria e esvazia os dicionarios a serem utilizados
            vals_supplier = {}
            #vals_order = {}
            
            file_name = next(tempfile._get_candidate_names()) + '.xml'
            file_path = tempfile.gettempdir()+'/'+ file_name +'.xml'
            data = chain.input_file

            tp_encode = chardet.detect(data.decode('base64'))
            
            if tp_encode.get('encoding',False):
                _logger.info('Encoding da NFe [%s]' % str(tp_encode['encoding']))
                if tp_encode['encoding'] != 'utf-8' and tp_encode['encoding'] != 'ascii' and tp_encode['encoding'] != 'UTF-8' and tp_encode['encoding'] != 'ASCII':
                    raise UserError(u'Codificação do Arquivo de XML %s' % tp_encode['encoding'],
                                     u'Converta o arquivo de XML para a codificação ASCII ou UTF-8.\n Para maiores informações entre em contato com o suporte.')
            
            data_danfe = chain.danfe_file
            
            f = open(file_path,'wb')
            f.write(data.decode('base64'))
            f.close()

            try:
                xmldoc = minidom.parse(file_path)
            except ValueError:
                return False
                

            nfeProc = ElementByTag(xmldoc,"nfeProc",0)
            DImp = ElementByTag(xmldoc,"declaracaoImportacao",0)
            
            if not nfeProc and not DImp:
                raise UserError(u'Não é um XML válido.',u'O arquivo inserido não é um arquivo XML válido.\nSomente é possível importar NFe autorizada pelo SEFAZ ou D.I. válida.')

            if DImp:
                pass
            else:
    
                vlProd = float(xmldoc.getElementsByTagName("vProd")[0].firstChild.data)
                if not vlProd or vlProd <= 0:
                    raise UserError(u'NFe sem produtos',u'Não é possível importar NFe sem produto ou de ajuste de impostos.')
    
                versao = nfeProc.getAttribute("versao")
    
                NFe = ElementByTag(xmldoc, "NFe", 0)
                infNFe = ElementByTag(NFe,"infNFe", 0)
    
                ide = ElementByTag(infNFe,"ide", 0)
                
                dEmi = ElementByTag(ide,"dhEmi", 0, True)
                dEntSai = ElementByTag(ide,"dhSaiEnt", 0, True) or dEmi
                
                if dEmi:
                    chain.dataEmissao = dEmi[0:10]
                
                serie = ElementByTag(ide,"serie", 0, True)
                
                nNF = ElementByTag(ide,"nNF", 0, True)
                
                if nNF:
                    chain.numeronNF = nNF
    
                if chain.name:
                    if nNF != chain.name:
                        chain.state = 'erro'
                        raise UserError(u'Número da Fatura não confere!',u'A Fatura de número %s, não confere com o número indicado %s.' % (nNF,chain.name))
                else:
                    chain.name = nNF
                dest = ElementByTag(infNFe,"dest", 0)
                cnpjDest = ElementByTag(dest,"CNPJ", 0, True)
                
                # verifica se empresa destinatária está logada.
                cnpj_user = self.env.user.company_id.cnpj_cpf
                cnpj_user = re.sub('[^0-9]', '', cnpj_user)
                if cnpj_user:
                    if cnpjDest != cnpj_user:
                        raise UserError(u'Arquivo não pertence a empresa logada.')
                    
                emit = ElementByTag(infNFe,"emit", 0)
                cnpj = ElementByTag(emit,"CNPJ", 0, True)
                
                supplier = ElementByTag(emit,"xNome", 0, True)
                fantasia = ElementByTag(emit,"xFant", 0, True) or supplier 
                    
                ie = ElementByTag(emit,"IE", 0, True)
                
                # Código do Regime Tributário
                crt = ElementByTag(emit,"CRT", 0, True)
                
                enderEmit = ElementByTag(infNFe,"enderEmit", 0)
                street = ElementByTag(enderEmit,"xLgr", 0, True)
                nro = ElementByTag(enderEmit,"nro", 0, True)
                district = ElementByTag(enderEmit,"xBairro", 0, True)
                city = ElementByTag(enderEmit,"xMun", 0, True)
                city_id = ElementByTag(enderEmit,"cMun", 0, True)
                state = ElementByTag(enderEmit,"UF", 0, True)
                zip = ElementByTag(enderEmit,"CEP", 0, True)
                country = ElementByTag(enderEmit,"cPais", 0, True) or 1058
                phone = ElementByTag(enderEmit,"fone", 0, True)
                
                infAdicional = ElementByTag(infNFe,"infAdic", 0)
                infCpl = ElementByTag(infAdicional,"infCpl", 0, True)
                if infCpl:
                    infCpl = infCpl + chr(13)
    
                #TODO2 partner_fiscal_type = self.env['l10n_br_account.partner.fiscal.type'].search([('code','=','Contribuinte')],limit=1)
                #ft_ids                 = self.env['l10n_br_account.partner.fiscal.type'].search([('default', '=', 'True'), ('is_company', '=', is_company)],limit=1)
                
                vals_supplier = {'name': fantasia.lower().title(),
                                 'legal_name': supplier.lower().title(),
                                 'street': street.lower().title(),
                                 'number': nro,
                                 'district': district.lower().title(),                
                                 'city_id': city_id,
                                 'city': city,
                                 'state': state,
                                 'zip': zip,
                                 'country_id': country,
                                 'phone': phone,
                                 'cnpj_cpf': cnpj,
                                 'supplier': True,
                                 'is_company': True,
                                 'inscr_est': ie,
                                 #TODO2.1 'partner_fiscal_type_id': partner_fiscal_type.id,
                }
    
                if infCpl:
                    if isinstance(infCpl, unicode):
                        infCpl = unicode(infCpl).replace('|',chr(13)+chr(10))
                    else:
                        infCpl = str(infCpl).replace('|',chr(13)+chr(10))
                if enderEmit.getElementsByTagName("xCpl"):
                    street2 = enderEmit.getElementsByTagName("xCpl")[0].firstChild.data
                    vals_supplier['street2'] = street2
                supplier = self.get_supplier(vals_supplier)
                
                if self.purchase_id:
                    if self.purchase_id.partner_id.id != supplier.id:
                        raise UserError(u'A Fatura não pertence ao fornecedor do pedido!',u'A Fatura de número %s, não pertence ao fornecedor do seu pedido de compra.' % (nNF))

                #_logger.info("fornecedor: "+supplier.name)
                #if supplier.property_account_position:
                #    vals_order['fiscal_position'] = supplier.property_account_position.id
                #    if supplier.property_account_position.fiscal_category_id:
                #        vals_order['fiscal_category_id'] = supplier.property_account_position.fiscal_category_id.id
                #if supplier.property_supplier_payment_term:
                #    vals_order['payment_term_id'] = supplier.property_supplier_payment_term.id
            
                chNFe = nfeProc.getElementsByTagName("chNFe")[0].firstChild.data
                itemlist = xmldoc.getElementsByTagName("det")
                infProt = xmldoc.getElementsByTagName("infProt")[0]
                if infProt:
                    protocolo = infProt.getAttribute("Id")
    
                vals_invoice = {
                    'name': False,                    
                    'origin': False,
                    'partner_id': supplier.id,
                    'account_id': supplier.property_account_payable_id.id,
                    'fiscal_position': False,
                    #'fiscal_category_id': False,
                    'payment_term': self.term_payment_id.id,
                    'num_fatura': nNF,
                    'internal_number': nNF,
                    'serie': serie,
                    'chave': chNFe,
                    'data_emissao': dEmi,
                    'data_Ent_Sai': dEntSai,
                    'versao': versao,
                    'protocolo': protocolo,
                    'observacao': infCpl,
                    'check_total': 0.0,
                }
     
                fiscal_position = False
                if chain.fiscal_position:
                    fiscal_position = chain.fiscal_position
#                 elif supplier.property_account_position_id:
#                     if supplier.property_account_position_id.type == 'input':
#                         fiscal_position = supplier.property_account_position_id
#                 if not fiscal_position:
#                     fiscal_position = FiscalPosObj.search([('type','=','input')],limit=1)
            
                if fiscal_position:
                    vals_invoice['fiscal_position'] = fiscal_position.id  
                    #TODO 5 if fiscal_position.fiscal_category_id:
                        #vals_invoice['fiscal_category_id'] = fiscal_position.fiscal_category_id.id  
               
                if chain.purchase_id:
                    vals_invoice['name'] = chain.purchase_id.name + ' '
                    vals_invoice['origin'] = chain.purchase_id.name
                if vals_invoice['name']:
                    vals_invoice['name'] = vals_invoice.get('name') + nNF
                else:
                    vals_invoice['name'] = nNF
                if supplier.property_account_position_id:
                    vals_invoice['fiscal_position'] = supplier.property_account_position_id.id
#                 if supplier.property_account_position_id.fiscal_category_id:
#                     vals_invoice['fiscal_category_id'] = supplier.property_account_position_id.fiscal_category_id.id
                if supplier.property_supplier_payment_term_id:
                    vals_invoice['payment_term'] = supplier.property_supplier_payment_term_id.id
                 
                txDesc = 0.0
                vDesc = xmldoc.getElementsByTagName("vDesc")[0]
                if vDesc:
                    vlProd = float(xmldoc.getElementsByTagName("vProd")[0].firstChild.data)
                    vlDesc = float(vDesc.firstChild.data)
                    txDesc = (vlDesc / vlProd) * 100 
                 
                Linhas = [] 
                seq = 10
                msg = False
                for itens in itemlist:
                    #print "*****itens*****"
                    #if itens.hasAttribute("xProd"):
                    imposto = itens.getElementsByTagName("imposto")[0]
                    codProd = itens.getElementsByTagName("cProd")[0].firstChild.data
                    produto = itens.getElementsByTagName("xProd")[0].firstChild.data
                    ncm = itens.getElementsByTagName("NCM")[0].firstChild.data
                    ean = itens.getElementsByTagName("cEAN")[0]
                    nr_item = itens.getAttribute("nItem")
                    if itens.getElementsByTagName("CEST"):
                        cest = itens.getElementsByTagName("CEST")[0].firstChild.data
                    else:
                        cest = False
                    uom = itens.getElementsByTagName("uCom")[0].firstChild.data
                    qtde = itens.getElementsByTagName("qCom")[0].firstChild.data
                    vlr = itens.getElementsByTagName("vUnCom")[0].firstChild.data
                    #vlTot = itens.getElementsByTagName("vProd")[0].firstChild.data
                    #impostos
                    val_imp = {}
                    # ICMS
                    ICMS = imposto.getElementsByTagName("ICMS")
                    if ICMS:
                        origem = '0'
                        if ICMS[0].getElementsByTagName("orig"):
                            origem = ICMS[0].getElementsByTagName("orig")[0].firstChild.data
                        if crt == "3":
                            icms_cst = ICMS[0].getElementsByTagName("CST")
                            if icms_cst:
                                icms = {
                                    "CST": icms_cst[0].firstChild.data,
                                }
                                val_imp["ICMS"] = icms
                        else:
                            icms_cst = ICMS[0].getElementsByTagName("CSOSN")
                            if icms_cst:
                                icms = {
                                    "CST": icms_cst[0].firstChild.data,
                                }
                                val_imp["ICMS"] = icms
                            
                    # IPI
                    IPI = imposto.getElementsByTagName("IPI")
                    if IPI:
                        ipi_CST = IPI[0].getElementsByTagName("CST")
                        ipi = {
                            "CST":  ipi_CST[0].firstChild.data,
                            "vBC":  0.0,
                            "pIPI": 0.0,
                            "vIPI": 0.0,
                        }
                        ipi_vBC     = IPI[0].getElementsByTagName("vBC")
                        ipi_pIPI    = IPI[0].getElementsByTagName("pIPI")
                        ipi_vIPI    = IPI[0].getElementsByTagName("vIPc.I")
                        if ipi_vBC:
                            ipi["vBC"]  = ipi_vBC[0].firstChild.data
                        if ipi_pIPI:
                            ipi["pIPI"] = ipi_pIPI[0].firstChild.data
                            ipi["uIPI"] = float(vlr) * (float(ipi["pIPI"])/100)
                             
                        if ipi_vIPI:
                            ipi["vIPI"] = ipi_vIPI[0].firstChild.data
                            
                        val_imp["IPI"] = ipi
                        
                    # PIS
                    PIS = imposto.getElementsByTagName("PIS")
                    if PIS:
                        pis_cst = PIS[0].getElementsByTagName("CST")[0].firstChild.data
                        if pis_cst:
                            pis = {
                                "CST": pis_cst,
                            }
                            val_imp["PIS"] = pis
                    # COFINS
                    COFINS = imposto.getElementsByTagName("COFINS")
                    if COFINS:
                        cofins_cst = COFINS[0].getElementsByTagName("CST")[0].firstChild.data
                        if cofins_cst:
                            cofins = {
                                "CST": cofins_cst,
                            }
                            val_imp["COFINS"] = cofins
                    
                    #print "Prod. : %s-%s-%s-%s %s %s " % (produto, codProd, ncm, un, qtde, vlr)
                    val_prod = {
                        'name': produto,
                        'type': 'product',
                        'partner_id': supplier.id,
                        'ncm': ncm,
                        'cest': cest,
                        'uom': uom,
                        'sequencia': seq,
                        'qtdade': float(qtde or 0),
                        'unitario': float(vlr or 0),
                        'discount': float(txDesc or 0),
                        'fiscal_type': FiscalType,
                        'origin': origem,
                        'imposto': val_imp,
                        'fiscal_position': vals_invoice.get('fiscal_position'),
                        'nr_item': nr_item,
                    }
                    
                    if ean.firstChild:
                        val_prod['ean13'] = ean.firstChild.data
                    val_prod['default_code'] = str(codProd)
                    val_prod = self.get_product(val_prod,cadastra=self.new_product)
                    if val_prod.get('msg'):
                        if msg:
                            msg += '\n'+val_prod['msg']
                        else:
                            msg = val_prod['msg']
                    linha = self.prepara_invoice_line(val_prod)
                    #_logger.info('Linha: '+str(linha)) 
                    Linhas.append(linha)
                    seq += 10
                
                
                if msg:
                    raise UserError(msg)
                
                # Duplicatas
                #TODO 8 if not supplier.property_supplier_payment_term and xmldoc.getElementsByTagName("cobr"):
                    #Dup = xmldoc.getElementsByTagName("dup")
                    #if Dup:
                        #als_invoice["dupVenc"] = Dup[-1].getElementsByTagName("dVenc")[0].firstChild.data
                    
                res_inv = self.prepara_invoice(vals_invoice)
    
                # Transportador
                
                # Procura o Invoice pelo número da NF
                Invoice = InvoiceObj.search([('partner_id','=',res_inv['partner_id']),('vendor_number','=',nNF)],limit=1)
                
                if Invoice:
                    if Invoice.state not in ['draft','cancel']: 
                        raise UserError(u'Fatura já confirmada!',u'A Fatura de número %s, está confirmada.' % (nNF))
                    else:
                        if Invoice.state == 'cancel':
                            vals_invoice['state'] = 'draft'
                else:
                    if self.purchase_id:
                        Invoice = InvoiceObj.search([#('partner_id','=',self.purchase_id.partner_id.id),
                                                     ('origin','=',self.purchase_id.name),
                                                     ('state','in',['draft','cancel'])],
                                                    limit=1)
                        if Invoice and not vals_invoice.get('payment_term',False):
                            vals_invoice['payment_term'] = self.purchase_id.payment_term_id.id
                            vals_invoice["dupVenc"] = False
    
                arq = AtachObj.search([('name','=',chain.input_file_fname)])
                if arq:
                    #raise except_orm(u'Arquivo do XML já incluído!',u'Já existe um arquivo de XML no repositório com este nome\n Favor renomear ou excluir o anterior.')
                    arq.unlink()
                    
                arq = AtachObj.search([('name','=',chain.danfe_file_fname)])
                if arq:
                    #raise except_orm(u'Arquivo da DANFE já incluído!',u'Já existe um arquivo da DANFE no repositório com este nome\n Favor renomear ou excluir o anterior.')
                    arq.unlink()
    
                if not Invoice:
                    # Cria o Invoice com o dict res_inv
                    Invoice = InvoiceObj.create(res_inv)
                    if Invoice and self.purchase_id:
                        sql = 'INSERT INTO purchase_invoice_rel (purchase_id,invoice_id)'\
                              'VALUES (%s,%s)' % (self.purchase_id.id,Invoice.id)
                        self._cr.execute(sql)
                        self.invalidate_cache()
                else:
                    Invoice.write(vals_invoice)
                    Invoice.invoice_line_ids.unlink()
    
                if Invoice:
                    
                    chain.invoice_id = Invoice.id
                    
                    # Duplicatas
                    try:
                        Duplicatas = self.env['duplicata.mercantil']
                        Duplicatas.search([("account_id","=",Invoice.id)]).unlink() 
    
                        for dup in xmldoc.getElementsByTagName("dup"):  
        
                            #nDup = dup.getElementsByTagName("nDup")[0].firstChild.data 
                            dVenc = dup.getElementsByTagName("dVenc")[0].firstChild.data
                            vDup = dup.getElementsByTagName("vDup")[0].firstChild.data 
        
                            duplicata = {
                                'account_id': Invoice.id,
                                'data_vencimento': dVenc,
                                'valor': float(vDup),
                            }
                            
                            Duplicatas.create(duplicata)
                    except:
                        pass
    
                    # Total 
                    Total = xmldoc.getElementsByTagName("vNF")[0]
                    if Total:
                        vlTotalNFe = Total.firstChild.data
                        sql = "UPDATE account_invoice SET amount_total=%s where id=%s" % (str(vlTotalNFe),Invoice.id)    
                        self._cr.execute(sql)
                        self.invalidate_cache()
                    
                    # Acrescenta as Linhas do Invoice
                    # TODO 9
                    for ln in Linhas:
                        ln['invoice_id'] = Invoice.id
                        if not ln['account_id']:
                            categoria = CategoriaObj.search([('parent_id','=',None)], limit=1)
                            if categoria[0].property_account_expense_categ_id: 
                                ln['account_id'] = categoria[0].property_account_expense_categ_id.id
                        fiscal_classification_id = ln.get('fiscal_classification_id')
                        line = InvoiceLineObj.create(ln)
                        
                        if fiscal_classification_id:
                            sql = "UPDATE account_invoice_line SET fiscal_classification_id=%s where id=%s" % (fiscal_classification_id,line.id)
                            #print sql
                            self._cr.execute(sql)
                            self.invalidate_cache()
    
                        if self.purchase_id:
                            orderline = self.env['purchase.order.line'].search([('order_id','=',self.purchase_id.id),('product_id','=',line.product_id.id)])
                            if orderline:
                                sql = 'INSERT INTO purchase_order_line_invoice_rel (order_line_id,invoice_id)'\
                                      'VALUES (%s,%s)' % (orderline.id,line.id)
                                self._cr.execute(sql)
                                orderline.invoiced = True
                                orderline.sudo().write({})
                    
                if Invoice:    
                    # Atach o Arquivo XML no Invoice
                    Vals_atach = {
                        'name': chain.input_file_fname,
                        'db_datas': data,
                        'datas_fname': chain.input_file_fname,
                        'res_model': InvoiceObj._name,
                        'res_id': Invoice.id,
                        'partner_id': Invoice.partner_id.id,
                        'user_id': self.user_id.id,
                        'company_id': self.user_id.company_id.id,
                        'description': 'new12',
                        'type': 'binary',
                    }
                    AtachObj.sudo().create(Vals_atach)
                    if data_danfe:
                        
                        Vals_danfe_atach = {
                            'name': chain.danfe_file_fname,
                            'db_datas': data_danfe,
                            'datas_fname': chain.danfe_file_fname,
                            'res_model': InvoiceObj._name,
                            'res_id': Invoice.id,
                            'partner_id': Invoice.partner_id.id,
                            'user_id': self.user_id.id,
                            'company_id': self.user_id.company_id.id,
                            'description': 'new13',
                            'type': 'binary',
                        }
                        AtachObj.sudo().create(Vals_danfe_atach)
            chain.state = 'done'
                            
            return {
                'name': 'Purchase Wizard',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'account.invoice',
                'res_id': Invoice.id,
                #'context': "{'journal_id': %d}" % (data['journal_id'],),
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
#         return {}