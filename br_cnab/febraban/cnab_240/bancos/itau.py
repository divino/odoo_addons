# -*- coding: utf-8 -*-
# © 2015 Luis Felipe Mileo
#        Fernando Marcato Rodrigues
#        Daniel Sadamo Hirayama
#        KMEE - www.kmee.com.br
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).


from ..cnab_240 import Cnab240


class Itau240(Cnab240):

    def __init__(self):
        super(Cnab240, self).__init__()
        from cnab240.bancos import itau
        self.bank = itau

    def _prepare_header(self):
        vals = super(Itau240, self)._prepare_header()
        vals['cedente_conta_dv']    = int(vals['cedente_conta_dv'])
        vals['cedente_agencia_dv']  = int(vals['cedente_agencia_dv'])
        vals['cedente_dv_ag_cc']    = int(vals['cedente_dv_ag_cc'])
        vals['data_credito']        = self.data_hoje()
        vals['cedente_convenio']    = u' '
        vals['vazio7']              = u'0000' 
        return vals

    def _prepare_segmento(self, line):
        vals = super(Itau240, self)._prepare_segmento(line)
        dv = self.dv_nosso_numero(
            line.payment_mode_id.bank_account_id.bra_number,
            line.payment_mode_id.bank_account_id.acc_number,
            line.payment_mode_id.boleto_carteira,
            line.nosso_numero
        )
        vals['nosso_numero'] = int(line.nosso_numero)
        vals['nosso_numero_dv'] = dv
        vals['carteira_numero'] = int(self.order.payment_mode_id.boleto_carteira)
        vals['cedente_conta_dv'] = int(vals['cedente_conta_dv'])
        vals['cedente_agencia_dv'] = int(vals['cedente_agencia_dv'])
        vals['cedente_dv_ag_cc'] = int(vals['cedente_dv_ag_cc'])
        return vals

    def dv_nosso_numero(self, agencia, conta, carteira, nosso_numero):
        composto = "%4s%5s%3s%8s" % (agencia.zfill(4), conta.zfill(5),
                                     carteira.zfill(3), nosso_numero.zfill(8))
        return self.modulo10(composto)

    def remessa(self, order):
        # Campos do Itaú fora do padrão, vazios com exigência de zeros
        self.bank.registros.HeaderArquivo._campos_cls['vazio7'].valor = "00000"
        self.bank.registros.HeaderArquivo._campos_cls['vazio9'].valor = "000"
        res = super(Itau240,self).remessa(order)
        self.arquivo.lotes[0].trailer.cobrancasimples_quantidade_titulos = len(order.line_ids)
        self.arquivo.lotes[0].trailer.quantidade_registros = (len(order.line_ids) * 2) + 2
        self.arquivo.lotes[0].trailer.numero_aviso = u'00000000'
        self.arquivo.trailer.vazio2 = u'00000000'
        self.arquivo.trailer.totais_quantidade_registros = (len(order.line_ids) * 2) + 4
        return res
    
    @staticmethod
    def modulo10(num):
        if not isinstance(num, basestring):
            raise TypeError
        soma = 0
        peso = 2
        for c in reversed(num):
            parcial = int(c) * peso
            if parcial > 9:
                s = str(parcial)
                parcial = int(s[0]) + int(s[1])
            soma += parcial
            if peso == 2:
                peso = 1
            else:
                peso = 2

        resto10 = soma % 10
        if resto10 == 0:
            modulo10 = 0
        else:
            modulo10 = 10 - resto10

        return modulo10
