# -*- coding: utf-8 -*-
# © 2015 Luis Felipe Mileo
#        Fernando Marcato Rodrigues
#        Daniel Sadamo Hirayama
#        KMEE - www.kmee.com.br
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).


from ..cnab_240 import Cnab240
import re
import string
from decimal import Decimal


class Bradesco240(Cnab240):

    def __init__(self):
        super(Cnab240, self).__init__()
        from cnab240.bancos import bradesco
        self.bank = bradesco

    def _prepare_header(self):
        vals = super(Bradesco240, self)._prepare_header()
        vals['controlecob_numero'] = self.order.file_number
        vals['controlecob_data_gravacao'] = self.data_hoje()
        vals['servico_servico'] = 1
        vals['cedente_convenio'] = self.order.payment_mode_id.bank_account_id.codigo_convenio
        vals['cedente_dv_ag_cc'] = ' '
        return vals

    def _prepare_segmento(self, line):
        vals = super(Bradesco240, self)._prepare_segmento(line)
        vals['prazo_baixa'] = str(vals['prazo_baixa'])
        vals['desconto1_percentual'] = Decimal('0.00')
        vals['valor_iof'] = Decimal('0.00')
        # vals['cobrancasimples_valor_titulos'] = Decimal('02.00')
#         vals['identificacao_titulo_banco'] = int(
#             vals['identificacao_titulo_banco'])
        carteira    = str(self.order.payment_mode_id.boleto_carteira).zfill(3)
        nossonumero = str(line.nosso_numero).zfill(11)
        nossonumero_dg = self.dv_nosso_numero(carteira,nossonumero)

        cobranca_carteira = int(self.order.payment_mode_id.cobranca_carteira)
        
        vals['carteira'] = carteira
        vals['nossonumero'] = nossonumero+nossonumero_dg
        vals['identificacao_titulo_banco'] = carteira+'00000'+nossonumero+nossonumero_dg
        
        vals['cedente_conta_dv'] = str(vals['cedente_conta_dv'])
        vals['cedente_agencia_dv'] = str(vals['cedente_agencia_dv'])
        vals['cobranca_carteira'] = cobranca_carteira if cobranca_carteira else 1
        vals['cobranca_cadastramento'] = 1
        vals['cobranca_documentoTipo'] = '1'
        vals['cobranca_distribuicaoBloqueto'] = '2'
        vals['especie_titulo'] = 2
        vals['cedente_dv_ag_cc'] = ' '#str(vals['cedente_dv_ag_cc'])
        vals['cobranca_emissaoBloqueto'] = 2
        vals['agencia_cobradora'] = vals['cedente_agencia'] 
        vals['agencia_cobradora_dv'] = vals['cedente_agencia_dv']
        return vals

    # Override cnab_240.nosso_numero. Diferentes números de dígitos entre
    # CEF e Itau
    def nosso_numero(self, format):
        digito = format[-1:]
        carteira = format[:3]
        nosso_numero = re.sub(
            '[%s]' % re.escape(string.punctuation), '', format[3:-1] or '')
        return carteira, nosso_numero, digito

    def remessa(self, order):
        # Alterado o tipo do dado no objeto de "num"para "alfa"
        
        self.bank.registros.SegmentoP._campos_cls['identificacao_titulo_banco'].formato = u'alfa'
        
        # Campos do Itaú fora do padrão, vazios com exigência de zeros
        super(Bradesco240,self).remessa(order)
        
        self.arquivo.header.controle_banco = int(order.payment_mode_id.bank_account_id.bank_bic)
        self.arquivo.trailer.controle_banco = int(order.payment_mode_id.bank_account_id.bank_bic)
        for lote in self.arquivo.lotes:
            lote.header.controle_banco = int(order.payment_mode_id.bank_account_id.bank_bic)
            lote.trailer.controle_banco = int(order.payment_mode_id.bank_account_id.bank_bic)
            for evento in lote.eventos:
                for seg in evento.segmentos:
                    seg.controle_banco = int(order.payment_mode_id.bank_account_id.bank_bic)
#                     if seg.servico_segmento == 'P':
#                     seg.identificacao_titulo_banco = self.arquivo.   

        res = unicode(self.arquivo)
        return res

    def dv_nosso_numero(self, carteira, nosso_numero):
        resto2 = self.modulo11(carteira[-2:] + nosso_numero[-11:], 7, 1)
        digito = 11 - resto2
        if digito == 10:
            dv = 'P'
        elif digito == 11:
            dv = 0
        else:
            dv = digito
        return str(dv)

    @staticmethod
    def modulo11(num, base=9, r=0):
        if not isinstance(num, basestring):
            raise TypeError
        soma = 0
        fator = 2
        for c in reversed(num):
            soma += int(c) * fator
            if fator == base:
                fator = 1
            fator += 1
        if r == 0:
            soma = soma * 10
            digito = soma % 11
            if digito == 10:
                digito = 0
            return digito
        if r == 1:
            resto = soma % 11
            return resto
    
